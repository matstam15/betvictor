# Interview Task – Software Engineer-Java

Candidate: Matias Centracchio <mati.centracchio@gmail.com>

Date: 28/02/2019

### Known Issues
- "history" does not limit to 10 elements, it returns all 
- Time is expressing in nanosecs

### Improvements
- Add more test cases to around concurrency
- Add javadoc
- Tune up Configuration around Http Connection Pool and ThreadPool
- Add more documentation about how to run the app, the API, etc...