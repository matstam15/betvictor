package com.betvictor.test.interviewtask.repository;

import com.betvictor.test.interviewtask.model.RandomTextStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RandomTextStatsRepository extends JpaRepository<RandomTextStats, Long> {

}
