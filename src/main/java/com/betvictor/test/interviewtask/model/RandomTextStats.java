package com.betvictor.test.interviewtask.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
public class RandomTextStats {

    @JsonIgnore
    @Id
    @GeneratedValue
    private Long id;

    @JsonProperty("freq_word")
    private String freqWord;

    @JsonProperty("avg_paragraph_size")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal avgParagraphSize;

    @JsonProperty("avg_paragraph_processing_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal avgProcessingTime;

    @JsonProperty("total_processing_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal totalProcessingTime;

}
