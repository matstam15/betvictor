package com.betvictor.test.interviewtask.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ParagraphData {

    private long processingTime;
    private String[] words;

}
