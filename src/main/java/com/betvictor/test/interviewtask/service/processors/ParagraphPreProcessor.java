package com.betvictor.test.interviewtask.service.processors;

import com.betvictor.test.interviewtask.model.ParagraphData;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ParagraphPreProcessor implements IParagraphProcessor<List<String>, List<ParagraphData>> {

    protected static final String WORD_SEPARATOR = " ";

    private static ParagraphData processParagraph(String paragraph) {

        Instant start = Instant.now();
        String[] words =
                Stream.of(paragraph.split(WORD_SEPARATOR))
                        .parallel()
                        .filter(StringUtils::isNotEmpty)
                        .toArray(String[]::new);

        return ParagraphData.builder()
                .words(words)
                .processingTime(Duration.between(start, Instant.now()).toNanos())
                .build();
    }

    @Override
    public List<ParagraphData> process(List<String> data) {

        if (data == null)
            throw new IllegalArgumentException();

        return data.stream()
                .parallel()
                .map(ParagraphPreProcessor::processParagraph)
                .collect(Collectors.toList());
    }

}
