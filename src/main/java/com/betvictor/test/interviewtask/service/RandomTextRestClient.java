package com.betvictor.test.interviewtask.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Component
public class RandomTextRestClient {

    private static final String gibberishUri =
            "http://www.randomtext.me/api/giberish/p-{number_of_paragraphs}/{min_number_of_words_per_sentence}-{max_number_of_words_per_sentence}";

    private RestTemplate restTemplate;

    @Autowired
    public RandomTextRestClient(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public GibberishResponse gibberish(int noParagraphs, int minNoWordsPerSentence, int maxNoWordsPerSentence) {
        Map<String, String> params = new HashMap<>();
        params.put("number_of_paragraphs", String.valueOf(noParagraphs));
        params.put("min_number_of_words_per_sentence", String.valueOf(minNoWordsPerSentence));
        params.put("max_number_of_words_per_sentence", String.valueOf(maxNoWordsPerSentence));
        return restTemplate.getForObject(gibberishUri, GibberishResponse.class, params);
    }

    @Getter
    @Setter
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class GibberishResponse {

        private String type;
        private int amount;
        private String number;
        private String number_max;
        private String format;
        private String time;
        private String text_out;

    }
}
