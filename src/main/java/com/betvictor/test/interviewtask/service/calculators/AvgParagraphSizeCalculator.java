package com.betvictor.test.interviewtask.service.calculators;

import com.betvictor.test.interviewtask.model.ParagraphData;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.function.Function;

@Component
public class AvgParagraphSizeCalculator implements Function<List<ParagraphData>, BigDecimal> {

    @Override
    public BigDecimal apply(List<ParagraphData> paragraphsData) {
        if (paragraphsData == null)
            throw new IllegalArgumentException();

        if (paragraphsData.isEmpty())
            return BigDecimal.ZERO;

        long tmpResult = paragraphsData.stream()
                .parallel()
                .mapToLong(paragraphData -> paragraphData.getWords().length)
                .sum() / paragraphsData.size();

        return BigDecimal.valueOf(tmpResult).setScale(2, RoundingMode.HALF_EVEN);
    }
}
