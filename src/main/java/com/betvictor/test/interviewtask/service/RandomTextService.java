package com.betvictor.test.interviewtask.service;

import com.betvictor.test.interviewtask.model.RandomTextStats;
import com.betvictor.test.interviewtask.repository.RandomTextStatsRepository;
import com.betvictor.test.interviewtask.service.processors.ParagraphProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class RandomTextService {

    private RandomTextProcessor randomTextProcessor;
    private ParagraphProcessor paragraphProcessor;
    private RandomTextStatsRepository randomTextStatsRepository;

    @Autowired
    public RandomTextService(
            RandomTextProcessor randomTextProcessor,
            ParagraphProcessor paragraphProcessor,
            RandomTextStatsRepository randomTextStatsRepository) {
        this.randomTextProcessor = randomTextProcessor;
        this.paragraphProcessor = paragraphProcessor;
        this.randomTextStatsRepository = randomTextStatsRepository;
    }

    public RandomTextStats generateRandomTextStats(int paragraphStart, int paragraphEnd, int wordCountMin, int wordCountMax)
            throws ExecutionException, InterruptedException {

        List<CompletableFuture<List<String>>> futures = new LinkedList<>();
        for (int i = paragraphStart; i <= paragraphEnd; i++) {
            futures.add(randomTextProcessor.process(i, wordCountMin, wordCountMax));
        }

        // All the completable futures will be merged into one CompletableFuture.
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
        CompletableFuture<List<List<String>>> allCompletableFuture =
                allFutures
                        .thenApply(ignoreThis ->
                                futures
                                        .stream()
                                        .map(CompletableFuture::join)
                                        .collect(Collectors.toList())
                        );

        RandomTextStats stats = paragraphProcessor.process(
                allCompletableFuture.get().stream()
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList())
        );

        return persist(stats);
    }

    public List<RandomTextStats> history() {
        return randomTextStatsRepository.findAll();
    }

    private RandomTextStats persist(RandomTextStats randomTextStats) {
        return randomTextStatsRepository.save(randomTextStats);
    }


}
