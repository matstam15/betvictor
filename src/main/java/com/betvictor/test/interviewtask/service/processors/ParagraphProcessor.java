package com.betvictor.test.interviewtask.service.processors;

import com.betvictor.test.interviewtask.model.ParagraphData;
import com.betvictor.test.interviewtask.model.RandomTextStats;
import com.betvictor.test.interviewtask.service.calculators.AvgParagraphSizeCalculator;
import com.betvictor.test.interviewtask.service.calculators.AvgProcessingTimeCalculator;
import com.betvictor.test.interviewtask.service.calculators.MostFrequentWordCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Service
public class ParagraphProcessor implements IParagraphProcessor<List<String>, RandomTextStats> {

    private ParagraphPreProcessor paragraphPreProcessor;
    private MostFrequentWordCalculator mostFrequentWordCalculator;
    private AvgParagraphSizeCalculator avgParagraphSizeCalculator;
    private AvgProcessingTimeCalculator avgProcessingTimeCalculator;

    @Autowired
    public ParagraphProcessor(
            final ParagraphPreProcessor paragraphPreProcessor,
            final MostFrequentWordCalculator mostFrequentWordCalculator,
            final AvgParagraphSizeCalculator avgParagraphSizeCalculator,
            final AvgProcessingTimeCalculator avgProcessingTimeCalculator) {
        this.paragraphPreProcessor = paragraphPreProcessor;
        this.mostFrequentWordCalculator = mostFrequentWordCalculator;
        this.avgParagraphSizeCalculator = avgParagraphSizeCalculator;
        this.avgProcessingTimeCalculator = avgProcessingTimeCalculator;
    }

    public RandomTextStats process(List<String> paragraphs) {
        Instant start = Instant.now();

        List<ParagraphData> preProcessedParagraphs = paragraphPreProcessor.process(paragraphs);

        return RandomTextStats.builder()
                .freqWord(mostFrequentWordCalculator.apply(preProcessedParagraphs))
                .avgParagraphSize(avgParagraphSizeCalculator.apply(preProcessedParagraphs))
                .avgProcessingTime(avgProcessingTimeCalculator.apply(preProcessedParagraphs))
                .totalProcessingTime(BigDecimal.valueOf(Duration.between(start, Instant.now()).toNanos()).setScale(4, RoundingMode.HALF_EVEN))
                .build();
    }

}
