package com.betvictor.test.interviewtask.service.calculators;

import com.betvictor.test.interviewtask.model.ParagraphData;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class MostFrequentWordCalculator implements Function<List<ParagraphData>, String> {

    @Override
    public String apply(List<ParagraphData> paragraphsData) {

        if (paragraphsData == null)
            throw new IllegalArgumentException();

        Map<String, Long> wordFrequencyMap =
                paragraphsData.stream()
                        .parallel()
                        .flatMap(paragraphData -> Stream.of(paragraphData.getWords()))
                        .map(String::toLowerCase)
                        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        Map.Entry<String, Long> mostFreqWord = null;
        for (Map.Entry<String, Long> entry : wordFrequencyMap.entrySet()) {
            if (mostFreqWord == null || mostFreqWord.getValue() < entry.getValue()) {
                mostFreqWord = entry;
            }
        }

        return mostFreqWord != null ? mostFreqWord.getKey() : "";
    }
}
