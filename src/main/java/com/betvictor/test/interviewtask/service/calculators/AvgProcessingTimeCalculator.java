package com.betvictor.test.interviewtask.service.calculators;

import com.betvictor.test.interviewtask.model.ParagraphData;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.function.Function;

@Component
public class AvgProcessingTimeCalculator implements Function<List<ParagraphData>, BigDecimal> {

    @Override
    public BigDecimal apply(List<ParagraphData> paragraphsData) {

        if (paragraphsData == null)
            throw new IllegalArgumentException();

        double tmpResult = paragraphsData.stream()
                .parallel()
                .mapToLong(ParagraphData::getProcessingTime)
                .average()
                .orElse(0);

        return BigDecimal.valueOf(tmpResult).setScale(4, RoundingMode.HALF_EVEN);
    }

}
