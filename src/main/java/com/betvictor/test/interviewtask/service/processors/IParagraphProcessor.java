package com.betvictor.test.interviewtask.service.processors;

@FunctionalInterface
public interface IParagraphProcessor<T, V> {

    V process(T data);

}
