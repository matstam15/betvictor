package com.betvictor.test.interviewtask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
public class RandomTextProcessor {

    private RandomTextRestClient randomTextRestClient;

    @Autowired
    public RandomTextProcessor(RandomTextRestClient randomTextRestClient) {
        this.randomTextRestClient = randomTextRestClient;
    }

    private static String[] extractParagraphs(String inputStr) {
        if (inputStr == null)
            return new String[0];

        StringBuilder input = new StringBuilder(inputStr);

        if (!inputStr.startsWith("<p>") || !inputStr.contains("</p>\r"))
            throw new IllegalArgumentException("Input data is not structured as expected. Does not present <p> or </p>\\r");

        return input.substring("<p>".length(), input.length() - "</p>\\r".length()).split("\\.</p>\\r<p>");

    }

    @Async("threadPoolTaskExecutor")
    public CompletableFuture<List<String>> process(int noParagraphs, int minNoWordsPerSentence, int maxNoWordsPerSentence) {
        String[] result = extractParagraphs(randomTextRestClient.gibberish(noParagraphs, minNoWordsPerSentence, maxNoWordsPerSentence).getText_out());
        return CompletableFuture.completedFuture(Arrays.asList(result));
    }
}
