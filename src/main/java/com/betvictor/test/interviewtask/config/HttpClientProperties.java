package com.betvictor.test.interviewtask.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "http-client")
public class HttpClientProperties {

    // Determines the timeout in milliseconds until a connection is established.
    private int connectionTimeOut = 30000;

    // The timeout when requesting a connection from the connection manager.
    private int requestTimeout = 30000;

    // The timeout for waiting for data
    private int socketTimeOut = 60000;

    private int maxTotalConnections = 50;
    private int defaultKeepAliveTimeMillis = 20 * 1000;
    private int closeIdleConnectionWaitTimeSecs = 30;

}
