package com.betvictor.test.interviewtask.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadPoolConfiguration {

    @Autowired
    ThreadPoolProperties threadPoolProperties;

    @Bean
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(threadPoolProperties.getCorePoolSize());
        pool.setMaxPoolSize(threadPoolProperties.getMaxPoolSize());
        pool.setWaitForTasksToCompleteOnShutdown(true);
        //pool.setQueueCapacity(100);
        pool.setThreadNamePrefix(threadPoolProperties.getThreadNamePrefix());
        return pool;
    }
}
