package com.betvictor.test.interviewtask.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "rest-template")
class RestTemplateProperties {

    private int schedulerPoolSize;
    private String schedulerThreadNamePrefix;

}
