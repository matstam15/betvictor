package com.betvictor.test.interviewtask.controller;

import com.betvictor.test.interviewtask.model.RandomTextStats;
import com.betvictor.test.interviewtask.service.RandomTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Validated
@RestController()
@RequestMapping("/betvictor")
public class RandomTextController {

    private RandomTextService randomTextService;

    @Autowired
    public RandomTextController(
            RandomTextService randomTextService) {
        this.randomTextService = randomTextService;
    }

    @GetMapping(value = "/text", produces = "application/json; charset=UTF-8")
    public ResponseEntity<RandomTextStats> randomText(
            @RequestParam("p_start") @Min(0) int paragraphStart,
            @RequestParam("p_end") @Min(0) int paragraphEnd,
            @RequestParam("w_count_min") @Min(0) int wordCountMin,
            @RequestParam("w_count_max") @Min(0) int wordCountMax)
            throws ExecutionException, InterruptedException {

        if (paragraphEnd < paragraphStart) {
            throw new IllegalArgumentException("Parameters Error: p_start must be <= than p_end");
        }

        if (wordCountMax < wordCountMin) {
            throw new IllegalArgumentException("Parameters Error: w_count_min must be <= than w_count_max");
        }

        RandomTextStats stats =
                randomTextService.generateRandomTextStats(
                        paragraphStart,
                        paragraphEnd,
                        wordCountMin,
                        wordCountMax);

        return ResponseEntity.ok().body(stats);
    }

    @GetMapping(value = "/history", produces = "application/json; charset=UTF-8")
    public ResponseEntity<List<RandomTextStats>> history() {
        return ResponseEntity.ok().body(randomTextService.history());
    }
}
