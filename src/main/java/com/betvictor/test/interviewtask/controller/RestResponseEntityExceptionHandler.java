package com.betvictor.test.interviewtask.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<Map> handleValidationFailure(IllegalArgumentException ex) {
        final Map<String, Object> response = new HashMap<>();
        response.put("message", "The request contains errors");
        response.put("errors", ex.getMessage());
        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    public ResponseEntity<Map> handleValidationFailure(ConstraintViolationException  ex) {
        final Map<String, Object> response = new HashMap<>();
        response.put("message", "The request contains errors");
        response.put("errors", ex.getConstraintViolations()
                .stream()
                .map(it -> new HashMap<String, String>() {{
                    put(it.getPropertyPath().toString(), it.getMessage());
                }})
                .collect(Collectors.toList())
        );

        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    public ResponseEntity<Map> handleRuntimeExceptione(RuntimeException ex) {

        final Map<String, Object> response = new HashMap<>();
        response.put("message", "Internal Error");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

}