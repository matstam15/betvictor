package com.betvictor.test.interviewtask.controller;

import com.betvictor.test.interviewtask.model.RandomTextStats;
import com.betvictor.test.interviewtask.repository.RandomTextStatsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class RandomTextControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private RandomTextStatsRepository randomTextStatsRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void integrationTest() throws Exception {

        randomTextStatsRepository.deleteAll();

        //response is retrieved as MvcResult
        MvcResult mvcResult = mockMvc.perform(get("/betvictor/text?p_start=1&p_end=200&w_count_min=100&w_count_max=4000")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        RandomTextStats persistedItem = randomTextStatsRepository.findAll().get(0);
        RandomTextStats responseObject = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RandomTextStats.class);

        assertEquals(persistedItem.getAvgProcessingTime(), responseObject.getAvgProcessingTime().setScale(2, RoundingMode.HALF_DOWN));
        assertEquals(persistedItem.getAvgParagraphSize(), responseObject.getAvgParagraphSize().setScale(2, RoundingMode.HALF_DOWN));
        assertEquals(persistedItem.getFreqWord(), responseObject.getFreqWord());
        assertEquals(persistedItem.getTotalProcessingTime(), responseObject.getTotalProcessingTime().setScale(2, RoundingMode.HALF_DOWN));

    }

}