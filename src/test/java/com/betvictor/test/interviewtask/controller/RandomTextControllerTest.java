package com.betvictor.test.interviewtask.controller;

import com.betvictor.test.interviewtask.model.RandomTextStats;
import com.betvictor.test.interviewtask.service.RandomTextService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class RandomTextControllerTest {

    @MockBean
    RandomTextService randomTextService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testController_text() throws Exception {
        BigDecimal avgParagraphSize = BigDecimal.valueOf(5);
        BigDecimal avgProcessingTime = BigDecimal.valueOf(200);
        BigDecimal totalProcessingTime = BigDecimal.valueOf(300);
        String freqWord = "and";

        //building the mock response
        RandomTextStats stats = RandomTextStats.builder()
                .avgParagraphSize(avgParagraphSize)
                .avgProcessingTime(avgProcessingTime)
                .freqWord(freqWord)
                .totalProcessingTime(totalProcessingTime)
                .build();

        Mockito.when(randomTextService.generateRandomTextStats(anyInt(), anyInt(), anyInt(), anyInt()))
                .thenReturn(stats);

        //response is retrieved as MvcResult
        MvcResult mvcResult = mockMvc.perform(get("/betvictor/text?p_start=1&p_end=2&w_count_min=2&w_count_max=5")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(stats)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.freq_word", CoreMatchers.is(freqWord)))
                .andExpect(jsonPath("$.avg_paragraph_size", CoreMatchers.is(avgParagraphSize.toString())))
                .andExpect(jsonPath("$.avg_paragraph_processing_time", CoreMatchers.is(avgProcessingTime.toString())))
                .andExpect(jsonPath("$.total_processing_time", CoreMatchers.is(totalProcessingTime.toString())))
                .andReturn();


        //json response body is converted/mapped to the Java Object
        String jsonResponse = mvcResult.getResponse().getContentAsString();
        RandomTextStats statsCreated = new ObjectMapper().readValue(jsonResponse, RandomTextStats.class);

        assertNotNull(statsCreated);
        assertEquals(statsCreated.getAvgProcessingTime(), avgProcessingTime);
        assertEquals(statsCreated.getAvgParagraphSize(), avgParagraphSize);
        assertEquals(statsCreated.getFreqWord(), freqWord);
        assertEquals(statsCreated.getTotalProcessingTime(), totalProcessingTime);

    }


    @Test
    public void testController_textWithIllegalArgs_pStartBiggerThanpEnd() throws Exception {

        Map<String, Object> response = new HashMap<>();
        response.put("message", "The request contains errors");
        response.put("errors", "Parameters Error: p_start must be <= than p_end");

        //response is retrieved as MvcResult
        MvcResult mvcResult = mockMvc.perform(get("/betvictor/text?p_start=5&p_end=2&w_count_min=2&w_count_max=5")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(response))
        )
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", CoreMatchers.is(response.get("message"))))
                .andExpect(jsonPath("$.errors", CoreMatchers.is(response.get("errors"))))
                .andReturn();
    }

    @Test
    public void testController_textWithIllegalArgs_wCountMinBiggerThanwCountMax() throws Exception {

        Map<String, Object> response = new HashMap<>();
        response.put("message", "The request contains errors");
        response.put("errors", "Parameters Error: w_count_min must be <= than w_count_max");

        //response is retrieved as MvcResult
        MvcResult mvcResult = mockMvc.perform(get("/betvictor/text?p_start=1&p_end=2&w_count_min=22&w_count_max=5")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(response))
        )
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", CoreMatchers.is(response.get("message"))))
                .andExpect(jsonPath("$.errors", CoreMatchers.is(response.get("errors"))))
                .andReturn();

        System.out.println(mvcResult);
    }

    @Test
    public void testController_textWithIllegalArgs_negativeValues() throws Exception {

        Map<String, Object> response = new HashMap<>();
        response.put("message", "The request contains errors");
        //response.put("errors", "Parameters Error: p_start must be <= than p_end");

        //response is retrieved as MvcResult
        MvcResult mvcResult = mockMvc.perform(get("/betvictor/text?p_start=-1&p_end=2&w_count_min=2&w_count_max=5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", CoreMatchers.is(response.get("message"))))
                .andExpect(jsonPath("$.errors[0].['randomText.paragraphStart']", CoreMatchers.is("must be greater than or equal to 0")))
                .andReturn();
    }

    @Test
    public void testController_textRuntimException() throws Exception {

        Mockito.when(randomTextService.generateRandomTextStats(anyInt(), anyInt(), anyInt(), anyInt()))
                .thenThrow(new RuntimeException());

        //response is retrieved as MvcResult
        MvcResult mvcResult = mockMvc.perform(get("/betvictor/text?p_start=1&p_end=2&w_count_min=2&w_count_max=5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", CoreMatchers.is("Internal Error")))
                .andReturn();
    }
}