package com.betvictor.test.interviewtask.service;

import com.betvictor.test.interviewtask.InterviewTaskTestUtils;
import com.betvictor.test.interviewtask.model.RandomTextStats;
import com.betvictor.test.interviewtask.repository.RandomTextStatsRepository;
import com.betvictor.test.interviewtask.service.processors.ParagraphProcessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RandomTextServiceTest {

    RandomTextService underTest;

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private RandomTextProcessor randomTextProcessor;
    @Autowired
    private RandomTextStatsRepository randomTextStatsRepository;
    @Autowired
    private ParagraphProcessor paragraphProcessor;

    private MockRestServiceServer mockRestServiceServer;

    @Before
    public void setUp() throws JsonProcessingException {


//        response.setText_out("<p>Hello how however.</p>\r<p>you however.</p>\n");
//        this.mockRestServiceServer.expect(max(2), requestTo("http://www.randomtext.me/api/giberish/p-2/2-3"))
//                .andRespond(withSuccess(objectMapper.writeValueAsString(response), MediaType.APPLICATION_JSON));

        underTest = new RandomTextService(
                randomTextProcessor,
                paragraphProcessor,
                randomTextStatsRepository);
    }

    @Test
    public void testGenerateRandomTextStats() throws ExecutionException, InterruptedException, JsonProcessingException {

        mockRestServiceServer = MockRestServiceServer.createServer(restTemplate);

        RandomTextRestClient.GibberishResponse response = new RandomTextRestClient.GibberishResponse();
        response.setText_out("<p>Pleasantly gallantly however.</p>\r");
        response.setAmount(1);
        response.setNumber("2");
        response.setNumber_max("3");
        response.setFormat("p");
        response.setType("gibberish");

        this.mockRestServiceServer.expect(once(), requestTo("http://www.randomtext.me/api/giberish/p-1/2-3"))
                .andRespond(withSuccess(objectMapper.writeValueAsString(response), MediaType.APPLICATION_JSON));

        int paragraphStart = 1;
        int paragraphEnd = 1;
        int wordCountMin = 2;
        int wordCountMax = 3;

        String expectedMostFreqWord = "however";
        BigDecimal expectedAvgParagraphSize = InterviewTaskTestUtils.buildBigDecimal(3);

        RandomTextStats stats = underTest.generateRandomTextStats(paragraphStart, paragraphEnd, wordCountMin, wordCountMax);
        assertEquals(expectedAvgParagraphSize, stats.getAvgParagraphSize());
        assertEquals(expectedMostFreqWord, stats.getFreqWord());
        assertTrue(stats.getTotalProcessingTime().compareTo(stats.getAvgProcessingTime().divide(BigDecimal.valueOf(paragraphEnd))) > 0);

    }

    @Test
    public void history() {

        randomTextStatsRepository.deleteAll();

        RandomTextStats stats = new RandomTextStats().builder()
                .avgParagraphSize(BigDecimal.ONE)
                .avgProcessingTime(BigDecimal.ONE)
                .freqWord("Yes")
                .totalProcessingTime(BigDecimal.valueOf(1000))
                .build();

        stats = this.randomTextStatsRepository.save(stats);
        List<RandomTextStats> allFound = underTest.history();

        Assert.assertEquals(1, allFound.size());
        Assert.assertEquals(stats.getId(), allFound.get(0).getId());
    }
}