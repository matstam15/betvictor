package com.betvictor.test.interviewtask.service.calculators;

import com.betvictor.test.interviewtask.InterviewTaskTestUtils;
import com.betvictor.test.interviewtask.model.ParagraphData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AvgProcessingTimeCalculatorTest {

    AvgProcessingTimeCalculator underTest;

    @Before
    public void setUp() {
        underTest = new AvgProcessingTimeCalculator();
    }

    @Test
    public void allParagraphsWithSameData() {
        int totalParagraphs = 100;
        // 100 paragraphs with the same data.
        List<ParagraphData> input = InterviewTaskTestUtils.generateData(totalParagraphs, 1000);

        long totalProcessingTime = input.get(0).getProcessingTime() * totalParagraphs;
        assertEquals(InterviewTaskTestUtils.buildBigDecimal(totalProcessingTime / totalParagraphs), underTest.apply(input));
    }


    @Test
    public void allParagraphsWithDiffData() {

        long defaultProcessingTime = 1000;
        List<ParagraphData> input = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            ParagraphData data = InterviewTaskTestUtils.buildParagraphData(2, defaultProcessingTime * (i + 1));
            input.add(data);
        }

        int totalParagraphs = input.size();
        long totalProcessingTime = input.stream().mapToLong(p -> p.getProcessingTime()).sum();
        assertEquals(InterviewTaskTestUtils.buildBigDecimal(totalProcessingTime / totalParagraphs), underTest.apply(input));
    }

    @Test
    public void testEmptyInput() {
        assertEquals(InterviewTaskTestUtils.buildBigDecimal(0), underTest.apply(new ArrayList<>()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void expectException() {
        underTest.apply(null);
    }


}