package com.betvictor.test.interviewtask.service.calculators;

import com.betvictor.test.interviewtask.InterviewTaskTestUtils;
import com.betvictor.test.interviewtask.model.ParagraphData;
import org.hamcrest.text.IsEqualIgnoringCase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class MostFrequentWordCalculatorTest {

    MostFrequentWordCalculator underTest;

    @Before
    public void setUp() {
        underTest = new MostFrequentWordCalculator();
    }

    @Test
    public void allParagraphsWithSameDataExceptOne() {
        int totalParagraphs = 5000;
        // 100 paragraphs with the same data.
        List<ParagraphData> input = InterviewTaskTestUtils.generateData(totalParagraphs, 2000);

        // now add the extra paragraph with a single word , which should make it the most frequent
        ParagraphData extraParagraphData = InterviewTaskTestUtils.buildParagraphData(1, 100);
        input.add(extraParagraphData);

        assertEquals(extraParagraphData.getWords()[0], underTest.apply(input));
    }

    @Test
    public void allParagraphsWithSameData_expectTheFirstWordProcessed() {
        int totalParagraphs = 5000;
        // 100 paragraphs with the same data.
        List<ParagraphData> input = InterviewTaskTestUtils.generateData(totalParagraphs, 2000);
        assertEquals(input.get(0).getWords()[0], underTest.apply(input));
    }


    @Test
    public void ignoreCaseSensitive() {

        String[] words = {"two", "three", "four", "one"};
        String[] ones = {"One", "one"};

        ParagraphData p1 = ParagraphData.builder().processingTime(1000).words(words).build();
        ParagraphData p2 = ParagraphData.builder().processingTime(1500).words(ones).build();
        List<ParagraphData> input = new ArrayList<>();
        input.add(p1);
        input.add(p2);

        assertThat(underTest.apply(input), IsEqualIgnoringCase.equalToIgnoringCase("One"));
    }

    @Test
    public void emptyInput() {
        List<ParagraphData> input = new ArrayList<>();
        assertEquals("", underTest.apply(input));
    }

    @Test(expected = IllegalArgumentException.class)
    public void expectException() {
        underTest.apply(null);
    }
}