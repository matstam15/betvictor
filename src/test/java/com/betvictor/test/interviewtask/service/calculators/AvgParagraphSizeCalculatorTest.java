package com.betvictor.test.interviewtask.service.calculators;

import com.betvictor.test.interviewtask.InterviewTaskTestUtils;
import com.betvictor.test.interviewtask.model.ParagraphData;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AvgParagraphSizeCalculatorTest {

    AvgParagraphSizeCalculator underTest;

    @Before
    public void setUp() {
        underTest = new AvgParagraphSizeCalculator();
    }

    @Test
    public void allParagraphsWithSameData() {
        int totalParagraphs = 100;
        // 100 paragraphs with the same data.
        List<ParagraphData> input = InterviewTaskTestUtils.generateData(totalParagraphs, 1000);

        int wordsPerParagraph = input.get(0).getWords().length;
        int totalWords = wordsPerParagraph * totalParagraphs;
        assertEquals(InterviewTaskTestUtils.buildBigDecimal(totalWords / totalParagraphs), underTest.apply(input));
    }


    @Test
    public void allParagraphsWithDiffData() {
        // 100 paragraphs with the same data.
        List<ParagraphData> input = new ArrayList<>();
        input.add(InterviewTaskTestUtils.buildParagraphData(1, 10));
        input.add(InterviewTaskTestUtils.buildParagraphData(2, 10));
        input.add(InterviewTaskTestUtils.buildParagraphData(3, 10));
        input.add(InterviewTaskTestUtils.buildParagraphData(4, 10));
        input.add(InterviewTaskTestUtils.buildParagraphData(5, 10));

        int totalParagraphs = input.size();
        int totalWords = input.stream().mapToInt(p -> p.getWords().length).sum();
        assertEquals(InterviewTaskTestUtils.buildBigDecimal(totalWords / totalParagraphs), underTest.apply(input));
    }

    @Test
    public void testEmptyInput() {
        assertEquals(BigDecimal.ZERO, underTest.apply(new ArrayList<>()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void expectException() {
        underTest.apply(null);
    }
}