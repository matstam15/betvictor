package com.betvictor.test.interviewtask.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RandomTextRestClientTest {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    private RandomTextRestClient client;
    @Autowired
    private ObjectMapper objectMapper;

    private MockRestServiceServer mockRestServiceServer;


    @Before
    public void setUp() throws Exception {

        mockRestServiceServer = MockRestServiceServer.createServer(restTemplate);


        RandomTextRestClient.GibberishResponse response = new RandomTextRestClient.GibberishResponse();
        response.setText_out("<p>Pleasantly gallantly however.</p>\r");
        response.setAmount(1);
        response.setNumber("2");
        response.setNumber_max("3");
        response.setFormat("p");
        response.setType("gibberish");


        String gibberishString =
                objectMapper.writeValueAsString(response);

        this.mockRestServiceServer.expect(requestTo("http://www.randomtext.me/api/giberish/p-1/2-3"))
                .andRespond(withSuccess(gibberishString, MediaType.APPLICATION_JSON));
    }

    @Test
    public void whenCallingGibberish_thenClientMakesCorrectCall()
            throws Exception {

        RandomTextRestClient.GibberishResponse response =
                this.client.gibberish(1, 2, 3);

        assertThat(response.getText_out()).isEqualTo("<p>Pleasantly gallantly however.</p>\r");
        assertThat(response.getAmount()).isEqualTo(1);
        assertThat(response.getNumber()).isEqualTo("2");
        assertThat(response.getNumber_max()).isEqualTo("3");
    }

}