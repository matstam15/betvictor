package com.betvictor.test.interviewtask.service.processors;

import com.betvictor.test.interviewtask.model.ParagraphData;
import com.betvictor.test.interviewtask.model.RandomTextStats;
import com.betvictor.test.interviewtask.service.calculators.AvgParagraphSizeCalculator;
import com.betvictor.test.interviewtask.service.calculators.AvgProcessingTimeCalculator;
import com.betvictor.test.interviewtask.service.calculators.MostFrequentWordCalculator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ParagraphProcessorTest {

    ParagraphProcessor underTest;

    @Mock
    private ParagraphPreProcessor mockParagraphPreProcessor;
    @Mock
    private MostFrequentWordCalculator mockMostFrequentWordCalculator;
    @Mock
    private AvgParagraphSizeCalculator mockAvgParagraphSizeCalculator;
    @Mock
    private AvgProcessingTimeCalculator mockAvgProcessingTimeCalculator;

    @Before
    public void setUp() {
        underTest = new ParagraphProcessor(
                mockParagraphPreProcessor,
                mockMostFrequentWordCalculator,
                mockAvgParagraphSizeCalculator,
                mockAvgProcessingTimeCalculator
        );
    }

    @Test
    public void shouldGenerateStats() {

        when(mockAvgParagraphSizeCalculator.apply(any())).thenReturn(BigDecimal.ONE);
        when(mockMostFrequentWordCalculator.apply(any())).thenReturn("YES");
        when(mockParagraphPreProcessor.process(any())).thenReturn(new ArrayList<ParagraphData>());
        when(mockAvgProcessingTimeCalculator.apply(any())).thenReturn(BigDecimal.valueOf(1000));

        RandomTextStats stats = underTest.process(new ArrayList<String>());

        assertEquals(BigDecimal.ONE, stats.getAvgParagraphSize());
        assertTrue(stats.getTotalProcessingTime().compareTo(BigDecimal.ZERO) > 0);
        assertEquals("YES", stats.getFreqWord());
        assertEquals(BigDecimal.valueOf(1000), stats.getAvgProcessingTime());

    }

    @Test(expected = IllegalArgumentException.class)
    public void expectException() {

        underTest = new ParagraphProcessor(
                new ParagraphPreProcessor(),
                mockMostFrequentWordCalculator,
                mockAvgParagraphSizeCalculator,
                mockAvgProcessingTimeCalculator
        );

        RandomTextStats stats = underTest.process(null);

    }

}