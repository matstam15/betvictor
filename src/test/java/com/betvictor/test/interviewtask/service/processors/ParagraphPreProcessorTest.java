package com.betvictor.test.interviewtask.service.processors;

import com.betvictor.test.interviewtask.model.ParagraphData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ParagraphPreProcessorTest {

    ParagraphPreProcessor underTest;

    @Before
    public void setUp() {
        underTest = new ParagraphPreProcessor();
    }

    @Test
    public void singleParagraph() {

        List<String> input = new ArrayList<>();
        input.add("one two three four five six");

        List<ParagraphData> output = underTest.process(input);

        assertEquals(output.size(), input.size());
        assertArrayEquals(output.get(0).getWords(), input.get(0).split(ParagraphPreProcessor.WORD_SEPARATOR));
    }

    @Test
    public void multipleParagraph() {

        List<String> input = new ArrayList<>();
        for (int i = 0; i < 5000; i++)
            input.add("one two three four five six " + i);

        List<ParagraphData> output = underTest.process(input);

        assertEquals(input.size(), output.size());
        for (ParagraphData paragraphData : output) {
            assertTrue(input.contains(
                    String.join(ParagraphPreProcessor.WORD_SEPARATOR, paragraphData.getWords())));
            assertTrue(paragraphData.getProcessingTime() > 0);
        }
    }

    @Test
    public void multipleSpacedParagraph() {
        List<String> input = new ArrayList<>();
        input.add("one          two ___ three          six");
        String[] expected = {"one", "two", "___", "three", "six"};

        List<ParagraphData> output = underTest.process(input);

        assertEquals(input.size(), output.size());
        assertArrayEquals(expected, output.get(0).getWords());
        assertEquals(expected.length, output.get(0).getWords().length);
    }

    @Test
    public void emptydParagraph() {
        List<String> input = new ArrayList<>();
        input.add("     ");

        List<ParagraphData> output = underTest.process(input);

        assertEquals(1, output.size());
        assertEquals(0, output.get(0).getWords().length);
        assertTrue(output.get(0).getProcessingTime() > 0);

    }


    @Test(expected = IllegalArgumentException.class)
    public void expectException() {
        underTest.process(null);
    }
}