package com.betvictor.test.interviewtask.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class RandomTextProcessorTest {

    RandomTextProcessor underTest;

    @Mock
    private RandomTextRestClient mockRandomTextRestClient;


    @Before
    public void setUp() {
        underTest = new RandomTextProcessor(
                mockRandomTextRestClient);
    }


    @Test
    public void testProcess() throws ExecutionException, InterruptedException {
        RandomTextRestClient.GibberishResponse response = new RandomTextRestClient.GibberishResponse();
        response.setText_out("<p>Pleasantly gallantly however.</p>\r<p>Ape more alas fumed that so since up.</p>\r<p>Along wow house.</p>\r");

        Mockito.when(mockRandomTextRestClient.gibberish(anyInt(), anyInt(), anyInt()))
                .thenReturn(response);

        List<String> result = underTest.process(3, 3, 8).get();

        Assert.assertThat(result.size(), equalTo(3));
        Assert.assertTrue(result.contains("Pleasantly gallantly however"));
        Assert.assertTrue(result.contains("Ape more alas fumed that so since up"));
        Assert.assertTrue(result.contains("Along wow house"));

    }

    @Test
    public void testProcessWithNullInput() throws ExecutionException, InterruptedException {
        RandomTextRestClient.GibberishResponse response = new RandomTextRestClient.GibberishResponse();
        response.setText_out(null);

        Mockito.when(mockRandomTextRestClient.gibberish(anyInt(), anyInt(), anyInt()))
                .thenReturn(response);

        List<String> result = underTest.process(3, 3, 8).get();

        Assert.assertThat(result.size(), equalTo(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void expectException() throws ExecutionException, InterruptedException {
        RandomTextRestClient.GibberishResponse response = new RandomTextRestClient.GibberishResponse();
        response.setText_out("wrong structured text");
        Mockito.when(mockRandomTextRestClient.gibberish(anyInt(), anyInt(), anyInt()))
                .thenReturn(response);
        List<String> result = underTest.process(3, 3, 8).get();
    }

}