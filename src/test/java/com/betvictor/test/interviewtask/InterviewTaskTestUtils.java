package com.betvictor.test.interviewtask;

import com.betvictor.test.interviewtask.model.ParagraphData;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class InterviewTaskTestUtils {

    public static String[][] wordArrays = new String[10][];

    public static String[] words_0 = {"zero"};
    public static String[] words_1 = Stream.concat(Arrays.stream(words_0), Arrays.stream(new String[]{"one"}))
            .toArray(String[]::new);
    public static String[] words_2 = Stream.concat(Arrays.stream(words_1), Arrays.stream(new String[]{"two"}))
            .toArray(String[]::new);
    public static String[] words_3 = Stream.concat(Arrays.stream(words_2), Arrays.stream(new String[]{"three"}))
            .toArray(String[]::new);
    public static String[] words_4 = Stream.concat(Arrays.stream(words_3), Arrays.stream(new String[]{"four"}))
            .toArray(String[]::new);
    public static String[] words_5 = Stream.concat(Arrays.stream(words_4), Arrays.stream(new String[]{"five"}))
            .toArray(String[]::new);
    public static String[] words_6 = Stream.concat(Arrays.stream(words_5), Arrays.stream(new String[]{"six"}))
            .toArray(String[]::new);
    public static String[] words_7 = Stream.concat(Arrays.stream(words_6), Arrays.stream(new String[]{"seven"}))
            .toArray(String[]::new);
    public static String[] words_8 = Stream.concat(Arrays.stream(words_7), Arrays.stream(new String[]{"eight"}))
            .toArray(String[]::new);
    public static String[] words_9 = Stream.concat(Arrays.stream(words_8), Arrays.stream(new String[]{"nine"}))
            .toArray(String[]::new);

    static {
        wordArrays[0] = words_0;
        wordArrays[1] = words_1;
        wordArrays[2] = words_2;
        wordArrays[3] = words_3;
        wordArrays[4] = words_4;
        wordArrays[5] = words_5;
        wordArrays[6] = words_6;
        wordArrays[7] = words_7;
        wordArrays[8] = words_8;
        wordArrays[9] = words_9;
    }

    public static List<ParagraphData> generateData(int size, long processingTime) {
        List<ParagraphData> ret = new ArrayList<>();
        for (int i = 0; i < size; i++)
            ret.add(buildParagraphData(10, 1000));
        return ret;
    }

    public static ParagraphData buildParagraphData(int size, long processingTime) {
        return ParagraphData.builder().words(wordArrays[size - 1]).processingTime(processingTime).build();
    }

    public static BigDecimal buildBigDecimal(double expected) {
        return new BigDecimal(expected).setScale(4, RoundingMode.HALF_EVEN);
    }

    public static BigDecimal buildBigDecimal(BigDecimal expected) {
        return expected.setScale(4, RoundingMode.HALF_EVEN);
    }

}
